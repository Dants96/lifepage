$("document").ready( function(){
    setTimeout(() => {
        $(".fadeInFirst").slideDown(1500, function(){
            $(".fadeInSecond").fadeIn(500);
            $(".fadeInProfilePicture").animate({
                opacity: 1.0
            });
        });
    }, 500);

});